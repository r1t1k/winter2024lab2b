import java.util.Scanner;
import java.util.Random;
public class Wordle{
	public static String generateWord() {
		//Generates a word that the user is going to attempt to guess.
		String[] possibleWords = new String[] {"FILES", "CHAIN", "SMOKE", "QUIRK", "SWIRL", "ZEBRA", "SHAKE", "THROW", "CHIMP", "GUILT", "CLOSE", "LAYER", "BREAK", "TWERK", "TRANQ", "PHONE", "HEADS", "TAILS", "WATCH", "SPLIT"};
		Random random = new Random();
		int randomIndex = random.nextInt(possibleWords.length);
		return possibleWords[randomIndex];
	}
	
	public static boolean letterInWord(String word, char c){
	//Check if the letter is in the word and returns whether it is in the word.
		for(int i = 0; i < word.length(); i++){
			if(word.charAt(i) == c)
				return true;
		}
		return false;
	}

	public static boolean letterInSlot(String word, char letter, int position){
		//Check if a letter is in the same spot in the word as the given position.
		return word.charAt(position)==letter;
	}
	
	public static String[] guessWord(String answer, String guess){
		//Compares 2 words and returns a color for each letter:
		// Yellow for in word but wrong spot, Red for not in word, and Green for in the word and right spot.
		String[] guessColor = new String[] {"Red", "Red", "Red", "Red", "Red"};
		for(int i = 0; i < answer.length();i++){
			if(letterInWord(answer, guess.charAt(i)) == true){
				if(letterInSlot(answer, guess.charAt(i), i) == true){
					guessColor[i] = "Green";
				}
				else{
					guessColor[i] = "Yellow";
				}
			}
		}
		return guessColor;
	}
	
	public static String presentResults(String word, String[] colours) {
		//Returns the guess with the colors from the previous function.
		final String ANSI_RESET = "\u001B[0m";
		final String ANSI_RED = "\u001B[31m";
		final String ANSI_GREEN = "\u001B[32m";
		final String ANSI_YELLOW = "\u001B[33m";
		String result = "";
		for(int i = 0; i < colours.length; i++){
			if(colours[i].equals("Green")){
				result += ANSI_GREEN + word.charAt(i) + ANSI_RESET;
			}
			if(colours[i].equals("Yellow")){
				result += ANSI_YELLOW + word.charAt(i) + ANSI_RESET;
			}
			if(colours[i].equals("Red")){
				result += ANSI_RED + word.charAt(i) + ANSI_RESET;
			}
		}
		return result;
	}
	
	public static String readGuess(){
		//Makes sure that the user has guessed a five letter word.
		System.out.println();
		Scanner reader = new Scanner(System.in);
        String userWordTarget= reader.nextLine();
		while(userWordTarget.length() != 5){
			System.out.println("\nThe word you guessed wasn't 5 letters.\nGuess another 5-Letter Word:");
			userWordTarget= reader.nextLine();
		}
		userWordTarget = userWordTarget.toUpperCase();
		return userWordTarget;
	}
	
	public static void playAgain(){
		//Asks the user if they want to play again and makes sure they say 'yes' or 'no'.
		Scanner scanner = new Scanner(System.in);
		String userInput = scanner.nextLine().toLowerCase();
		if (userInput.equals("yes")) {
            System.out.println("Thank you for playing my Game Again!!");
			runGame(generateWord());
        } 
		else{ 
			if (userInput.equals("no")) {
				System.out.println("Haaaa!! I knew you weren't smart enough for this game\nAnyways Thanks for playing my Game.\nI hope you had a good time😉.\nEnjoy the rest of your short life.");
			}
			else{
				System.out.println("Bruh its 2 or 3 letters. Not that difficult to type that out. \nSkill issue for real. \nEnter 'yes' or 'no' again and right this time.");
				playAgain();
			}
        }
	}
		
	public static void runGame(String target){
		//Makes the user guess for a random 5-letter word with 6 attempts.
		// And for every guess, returns the guess with the colors indicating which letters are in the word or in the right position.
		//It also makes the user play again if they lost and if they won, it promts them whether they would like to play again.
		int attempts = 6;
		boolean win = false;
		while(attempts > 0 && win == false){
			System.out.println("\nGuess The 5-Letter Word:");
			String userGuess = readGuess();
			System.out.println(presentResults(userGuess, guessWord(target, userGuess)));
			if (userGuess.equals(target)){
				win = true;
			}
			attempts--;
			if(win){
				System.out.println("Great job, you win, it only took you " + (6-attempts) + " tries.");
				System.out.println("\nWould you like to play again?(yes/no)\n");
				playAgain();
			}
		}
		if(attempts == 0 && win == false){
			System.out.println("No way you didn't get it.\nL wordle player. \nJit needs to get better at words.\nThe word was " + target + "\nTry Again");
			runGame(generateWord());
		}
	}
}