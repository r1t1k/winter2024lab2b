import java.util.Scanner;
public class GameLauncher{
	public static void main(String[] args){
		//Welcomes the user, Gives them the choices they have and runs the gameRunner method
		System.out.println("Hello There.\nWelcome to my games.\nHere are 2 of my games that you can play today:\n1) Hangman\n2) Wordle\nType either 1 for Hangman or 2 for Wordle or 3 to Quit.");
		gameRunner();		
	}
	
	public static void gameRunner(){
		Scanner reader = new Scanner(System.in);
		int chosenGame = reader.nextInt();
		if(chosenGame == 1){	//Checks if the user chose Hangman and runs Hangman then asks them if they want to play again.
			runHangman();
			playAgain();		
		}
		else if(chosenGame == 2){	//Checks if the user chose Wordle and runs Wordle then asks them if they want to play again.
			runWordle();
			playAgain();
		}
		else if(chosenGame == 3){	//Checks if the user chose to quitand ends the game.
			System.out.println("Thank you for Playing. Hope to see you again.");
		}
		else{ 						//If the user did not enter any of the 3 choices, asks them to re-enter.
			System.out.println("Please enter only a '1' or a '2'");
			gameRunner();
		}
	}
	
	public static void playAgain(){
		System.out.print("\033c");
		//Clears the terminal and asks the user if they want to play again or quit.
		System.out.println("Would you like to play again or play the a different game?\nType yes to play again and no to quit.");
		Scanner reader = new Scanner(System.in);
		String wantToPlayAgain = reader.nextLine();
		//takes user input and either runs the program again or ends the program.
		if(wantToPlayAgain.equals("yes")){
			gameRunner();
		}
		else{
			System.out.println("Thank you for Playing. Have a good one. Hope to see you agian.");
		}
	}
	
	public static void runHangman(){
		System.out.print("\033c");
		//Clears the terminal to start the Hangman game.
		System.out.println();		
		Scanner reader = new Scanner(System.in);
        System.out.print("Welcome to Hangman\nGive the WORD you want your Friend to Guess: ");
        String userWordTarget= reader.nextLine();
		//Takes user's word that they want their friend to guess, makes it uppercased and clears the screen for the friend to guess the word with only 6 misses.
		userWordTarget = userWordTarget.toUpperCase();
		System.out.print("\033c");
		Hangman.runGame(userWordTarget);
	}
	
	public static void runWordle(){
		System.out.print("\033c");
		//Clears the terminal to start the Wordle game, welcomes the user and runs the Wordle game..
		System.out.println();		
        System.out.print("Welcome to Wordle\n");
		Wordle.runGame(Wordle.generateWord());
	}
}